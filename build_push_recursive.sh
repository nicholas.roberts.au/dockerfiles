#!/bin/sh
for dir in $(ls .)
do
	docker build -t hplustime/$dir $dir
	docker push hplustime/$dir
done
